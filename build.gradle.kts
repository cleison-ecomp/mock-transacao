plugins {
    war
    id("org.gretty") version "3.0.3"
    eclipse
	id("org.sonarqube") version "3.0"
}

repositories {
    jcenter() 
}

dependencies {
    providedCompile("javax.servlet:javax.servlet-api:4.0.1")

    testImplementation("junit:junit:4.13")
}

tasks.compileJava {
    options.release.set(8)
}

gretty {
    contextPath = "/"
    servletContainer = "jetty9"
}