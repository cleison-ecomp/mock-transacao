# Mock Transação
Essa é uma aplicação feita em Java, utilizando o gradle para automação e contrução, cujo objetivo é fornecer uma API que retorna uma lista de transações, geradas automaticamente, no formato JSON.

## Obtendo as transações
	[GET] /<id>/transacoes/<ano>/<mes>

## Pré-requisito
É necessário uma versão Java 1.8+

## Para executar localmente
Clone e execute o comando abaixo:

	gradlew jettyRunWar
A API estará disponível em http://localhost:8080/

## Como importar para o eclipse
Caso deseje, execute o comando abaixo para importar o projeto para o eclipse.
	
	gradlew eclipse


