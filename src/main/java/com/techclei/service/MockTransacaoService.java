package com.techclei.service;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.techclei.usecases.consultartransacao.ConsultarTransacaoController;
import com.techclei.usecases.criartransacao.CriarTransacaoController;

/**
 * Classe ponto de entrada para as requisições feitas para o sistema.
 * Aceita requisições http GET
 * @author Cleison
 *
 */
public class MockTransacaoService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String HEAD_MOTIVO_ERRO = "motivoErro";
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri;
		String[] parametros;
		Integer idUsuario = null;
		Integer ano = null;
		Integer mes = null;
		
		PrintWriter out = null;
		
		ConsultarTransacaoController consultarTransacaoController = new ConsultarTransacaoController();
		CriarTransacaoController criarTransacaoController = new CriarTransacaoController();
		
		try {
			// Configura tipo de conteúdo das respostas
			response.setContentType("application/json");
			// Configura encoding
			response.setCharacterEncoding("UTF-8");
			
			out = response.getWriter();
			
			uri = request.getRequestURI();
			
			// Resolve proxy em contexto de produção
			if(uri.contains("MockTransacao")) {
				uri = uri.replace("/MockTransacao", "");
			}
			parametros = uri.split("/");
			
			// validar o path do contrato
			if(parametros == null || parametros.length != 5 || !parametros[2].equals("transacoes")) {
				response.setStatus(404);
				response.setHeader(HEAD_MOTIVO_ERRO, "Recurso nao encontrado. Verifique sua URI");
				String json = "{\"erro\": true, \"msg\": \"Recurso não encontrado. Verifique sua URI\"}";
				out.println(json);
				out.flush();
				return;
			}

			// Cast para validar parâmetros recebidos
			try {
				idUsuario = Integer.parseInt(parametros[1]);
				ano = Integer.parseInt(parametros[3]);
				mes = Integer.parseInt(parametros[4]);

				if (idUsuario < 1000 || idUsuario > 100000) {
					response.setStatus(404);
					response.setHeader(HEAD_MOTIVO_ERRO, "Usuario nao encontrado.");
					String json = "{\"erro\": true, \"msg\": \"Usuário não encontrado.\"}";

					out.println(json);
					out.flush();
				} else if(ano < 1) {
					response.setStatus(400);
					response.setHeader(HEAD_MOTIVO_ERRO, "O ano informado é inválido.");
					String json = "{\"erro\": true, \"msg\": \"O ano informado é inválido.\"}";
					out.println(json);
					out.flush();
				} else if(mes < 1 || mes > 12) {
					response.setStatus(400);
					response.setHeader(HEAD_MOTIVO_ERRO, "O mes informado é inválido.");
					String json = "{\"erro\": true, \"msg\": \"O mês informado é inválido.\"}";
					out.println(json);
					out.flush();
				} else {
					String json = consultarTransacaoController.executar(idUsuario, ano, mes);
					if(json.isEmpty()) {
						json = criarTransacaoController.executar(idUsuario, ano, mes);
					}
					out.println(json);
					out.flush();
				}
				
			} catch (Exception ex) {
				response.setStatus(400);
				response.setHeader(HEAD_MOTIVO_ERRO, "Parametros mal formatados.");
				String json = "{\"erro\": true, \"msg\": \"Parâmetros mal formatados.\"}";
				out.println(json);
				out.flush();
			}
		} catch (Exception e) {
			if (out != null) {
				response.setStatus(500);
				response.setHeader(HEAD_MOTIVO_ERRO, "Ocorreu um erro interno.");
				String json = "{\"erro\": true, \"msg\": \"Ocorreu um erro interno.\"}";
				out.println(json);
				out.flush();
			}
		} finally {
			if (out != null)
				out.close();
		}
	}
}
