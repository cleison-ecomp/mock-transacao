package com.techclei.usecases.criartransacao;

import java.util.List;

import com.techclei.entities.FabricaListaTransacao;
import com.techclei.entities.Transacao;
import com.techclei.repositories.TransacaoRepository;

public class CriarTransacaoUseCase {
	private TransacaoRepository transacaoRepository;
	
	public CriarTransacaoUseCase(TransacaoRepository transacaoRepository) {
		this.transacaoRepository = transacaoRepository;		
	}

	public List<Transacao> executar(Integer idUsuario, Integer ano, Integer mes) {
		List<Transacao> listaTransacoes = new FabricaListaTransacao().gerarListaTransacoes(idUsuario, ano, mes);
		this.transacaoRepository.salvarListaTransacoes(idUsuario, ano, mes, listaTransacoes);
		return listaTransacoes;
	}
}
