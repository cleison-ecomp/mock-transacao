package com.techclei.usecases.criartransacao;

import java.util.List;

import com.techclei.entities.FabricaListaTransacao;
import com.techclei.entities.Transacao;
import com.techclei.repositories.implementations.CacheTransacaoRepository;

public class CriarTransacaoController {
	
	private CriarTransacaoUseCase criarTransacaoUseCase;
	private CacheTransacaoRepository cacheTransacaoRepository;
	
	public CriarTransacaoController() {
		this.cacheTransacaoRepository = new CacheTransacaoRepository();
		this.criarTransacaoUseCase = new CriarTransacaoUseCase(cacheTransacaoRepository);
	}
	
	public String executar(Integer idUsuario, Integer ano, Integer mes) {
		List<Transacao> listaTransacoes = this.criarTransacaoUseCase.executar(idUsuario, ano, mes);
		
		return FabricaListaTransacao.converterTransacoesParaJson(listaTransacoes);
	}
}
