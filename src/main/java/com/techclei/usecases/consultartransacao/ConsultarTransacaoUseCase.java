package com.techclei.usecases.consultartransacao;

import java.util.List;

import com.techclei.entities.Transacao;
import com.techclei.repositories.TransacaoRepository;

public class ConsultarTransacaoUseCase {
	private TransacaoRepository transacaoRepository;
	
	public ConsultarTransacaoUseCase(TransacaoRepository transacaoRepository) {
		this.transacaoRepository = transacaoRepository;		
	}

	public List<Transacao> executar(Integer idUsuario, Integer ano, Integer mes) {
		return this.transacaoRepository.obterListaTransacoes(idUsuario, ano, mes);
	}
}
