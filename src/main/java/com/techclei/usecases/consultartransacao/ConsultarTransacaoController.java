package com.techclei.usecases.consultartransacao;

import java.util.List;

import com.techclei.entities.FabricaListaTransacao;
import com.techclei.entities.Transacao;
import com.techclei.repositories.implementations.CacheTransacaoRepository;

public class ConsultarTransacaoController {
	
	private ConsultarTransacaoUseCase consultarTransacaoUseCase;
	private CacheTransacaoRepository cacheTransacaoRepository;
	
	public ConsultarTransacaoController() {
		this.cacheTransacaoRepository = new CacheTransacaoRepository();
		this.consultarTransacaoUseCase = new ConsultarTransacaoUseCase(cacheTransacaoRepository);
	}
	
	public String executar(Integer idUsuario, Integer ano, Integer mes) {
		List<Transacao> listaTransacoes = this.consultarTransacaoUseCase.executar(idUsuario, ano, mes);
		if(listaTransacoes != null) {
			return FabricaListaTransacao.converterTransacoesParaJson(listaTransacoes);
		} else {
			return "";
		}
	}
}
