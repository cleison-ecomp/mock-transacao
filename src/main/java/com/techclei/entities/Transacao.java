package com.techclei.entities;

/**
 * Classe do modelo de negócio que representa uma transação do cliente,
 * contendo uma descrição, uma data e um valor.
 * @author Cleison
 *
 */
public class Transacao {
	private String descricao;
	private long data;
	private Integer valor;
	
	public Transacao(String descricao, long data, Integer valor) {
		this.descricao = descricao;
		this.data = data;
		this.valor = valor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public long getData() {
		return data;
	}
	public void setData(long data) {
		this.data = data;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
}
