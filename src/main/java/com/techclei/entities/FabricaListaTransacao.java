package com.techclei.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.techclei.util.JSONObject;

/**
 * Classe que agrega as operaçõeses de manutenção das transações.
 * Responsável por criar lista de transações.
 * 
 * @author Cleison
 *
 */
public class FabricaListaTransacao {
	private Random random = new Random();
	
	/**
	 * Método para gerar lista de transações com um total de itens igual ao primeiro dígito do id do usuário
	 * multiplicado pelo mês. 
	 * @param idUsuario Integer
	 * @param ano Integer
	 * @param mes Integer
	 * @return List
	 */
	public List<Transacao> gerarListaTransacoes(Integer idUsuario, Integer ano, Integer mes) {
		List<Transacao> listatrasancoes = new ArrayList<>();
		Integer primeiroDigito = Integer.parseInt(idUsuario.toString().substring(0, 1));
		Integer totalTransacoes = primeiroDigito * mes;
		Integer valorTransacao;
		String descricaoTransacao = "";
		long dataTransacao;
		
		
		for(int i = 0; i < totalTransacoes; i++) {
			descricaoTransacao = gerarDescricaoTransacao();
			dataTransacao = gerarDataTransacao(ano, mes);
			valorTransacao = gerarValorTransacao(idUsuario, i, mes);
			
			Transacao transacao = new Transacao(descricaoTransacao, dataTransacao, valorTransacao);
			listatrasancoes.add(transacao);
		}

		return listatrasancoes;
		
	}
	
	/**
	 * Gera descrições aleatórias legíveis para humanos.
	 * @return String
	 */
	public String gerarDescricaoTransacao() {
		Integer minimoCaracteres = 11;
		Integer maximoCaracteres = 60;
		Integer tamanhoPalavra;
		Integer tamanhoDescricao;
		Integer elegerVogal;
		Integer elegerConsoante;
		String[] vogais = {"a", "e", "i", "o", "u"};
		String[] consoantesDigrafos = {"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z",
				"ch", "lh", "nh", "rr", "ss", "sc", "xc", "xs"};
		StringBuilder palavra;
		StringBuilder descricao = new StringBuilder();
		
		tamanhoDescricao = this.random.nextInt((maximoCaracteres - minimoCaracteres) +1) + minimoCaracteres;
		while (descricao.length() < tamanhoDescricao) {
			tamanhoPalavra =  this.random.nextInt((5 - 2) + 1) + 2;
			palavra = new StringBuilder();
			for (int j = 0; j < tamanhoPalavra; j++) {
				elegerVogal = this.random.nextInt(vogais.length);
				elegerConsoante = this.random.nextInt(consoantesDigrafos.length);
				palavra.append(consoantesDigrafos[elegerConsoante].concat(vogais[elegerVogal]));
			}
			
			if((descricao.length() + palavra.length()) > tamanhoDescricao) {
				palavra.deleteCharAt((descricao.length() + palavra.length()) - tamanhoDescricao);
			}
			descricao.append(palavra);
			descricao.append(" ");
		}

		return descricao.toString().trim();
	}
	
	/**
	 * Gera data aleatórias do tipo timestamp, dentro do ano e mês informados.
	 * @param ano Integer
	 * @param mes Integer
	 * @return long
	 */
	public long gerarDataTransacao(Integer ano, Integer mes) {
		Calendar calendar = Calendar.getInstance();
		Integer maximoDiaMes;
		Integer dia;
		Integer hora;
		Integer minuto;
		Integer segundo;
		
		calendar.set(Calendar.YEAR, ano);
		calendar.set(Calendar.MONTH, mes-1);
		
		maximoDiaMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		dia = this.random.nextInt((maximoDiaMes - 1) + 1) + 1;
		hora = this.random.nextInt(24);
		minuto = this.random.nextInt(60);
		segundo = this.random.nextInt(60);
		
		calendar.set(Calendar.DAY_OF_MONTH, dia);
		calendar.set(Calendar.HOUR_OF_DAY, hora);
		calendar.set(Calendar.MINUTE, minuto);
		calendar.set(Calendar.SECOND, segundo);

		return calendar.getTimeInMillis();
	}
	
	/**
	 * Gera um valor inteiro com base no id do usuário, índice da lista de transaões e o mês: (idUsuario*índice)/mês.
	 * O valor será negativo quando o resto da divisão do índice por 2 for igual a 0.
	 * @param idUsuario Integer
	 * @param indice Integer
	 * @param mes Integer
	 * @return Integer
	 */
	public Integer gerarValorTransacao(Integer idUsuario, Integer indice, Integer mes) {
		Integer valor = 0;
		
		if(indice % 2 == 0) {
			valor = (((-1)*idUsuario*indice)/mes);
		} else {
			valor = (idUsuario*indice)/mes;
		}
		return valor;
	}
	
	/**
	 * Formata a lista de transações em JSON
	 * @param listaTransacoes List<Transacao>
	 * @return String - JSON
	 */
	public static String converterTransacoesParaJson(List<Transacao> listaTransacoes) {
		List<String> listaTransacoesJson = new ArrayList<>();
		JSONObject jsonObject;
		
		for (Transacao transacao : listaTransacoes) {
			jsonObject = new JSONObject();
			jsonObject.put("descricao", transacao.getDescricao());
			jsonObject.put("data", transacao.getData()+"");
			jsonObject.put("valor", transacao.getValor()+"");
			
			listaTransacoesJson.add(jsonObject.toString());
		}
		
		return listaTransacoesJson.toString();
	}

}
