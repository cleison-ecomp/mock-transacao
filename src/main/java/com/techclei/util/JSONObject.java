package com.techclei.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Classe para auxiliar na conversão de Objetos em String no padrão JSON.
 * @author Cleison
 *
 */
public class JSONObject {
	private HashMap<String, String> elementos = new HashMap<>();
	
	/**
	 * Cria um elemento tendo como chave o parâmetro nomeElemento e seu conteúdo associado pelo parâmetro valor.
	 * Chaves preexistentes terão seu valor atualizado.
	 * @param nomeElemento String
	 * @param valor String
	 */
	public void put (String nomeElemento, String valor) {
		this.elementos.put(nomeElemento, valor);
	}
	
	/**
	 * Retorna o valor associado a chave infomada ou null quando ela não existe.
	 * @param key String
	 * @return String ou null
	 */
	public String get (String key) {
		if(this.elementos.containsKey(key)) {
			return this.elementos.get(key);
		} else {
			return null;
		}
	}
	
	/**
	 * Converte chave e valor em um objeto JSON. 
	 * @return String
	 */
	public String toString() {
		String valor = "{";
		Set<Entry<String, String> > map  = elementos.entrySet();
		Iterator<Entry<String, String> > it = map.iterator();
		Integer i = 1;
		while(it.hasNext()) {
			Entry<String, String> entry = it.next();
			if(i == elementos.size()) {
				valor+="\""+entry.getKey()+"\":\""+escapeCaracteresEspeciais(entry.getValue())+"\"";
			} else {
				valor+="\""+entry.getKey()+"\":\""+escapeCaracteresEspeciais(entry.getValue())+"\",";
			}
			i ++;
		}
		valor+="}";
		return valor;
	}
	
	private String escapeCaracteresEspeciais (String valor) {
	    return valor.replace("\n","\\\\n")
	    			.replace("\r","\\\\r")
	    			.replace("\t","\\\\t");

	}
}
