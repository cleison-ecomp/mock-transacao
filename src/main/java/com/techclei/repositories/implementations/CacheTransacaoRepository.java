package com.techclei.repositories.implementations;

import java.util.List;

import com.techclei.entities.Transacao;
import com.techclei.repositories.TransacaoRepository;

public class CacheTransacaoRepository implements TransacaoRepository{

	@Override
	public List<Transacao> obterListaTransacoes(Integer idUsuario, Integer ano, Integer mes) {
		return CacheTransacaoCliente.getInstance().obterListaTransacoes(idUsuario, ano, mes);
	}

	@Override
	public void salvarListaTransacoes(Integer idUsuario, Integer ano, Integer mes, List<Transacao> listaTransacoes) {
		CacheTransacaoCliente.getInstance().salvar(idUsuario, ano, mes, listaTransacoes);
	}

}
