package com.techclei.repositories.implementations;

import java.util.HashMap;
import java.util.List;

import com.techclei.entities.Transacao;

/**
 * Essa classe  é uma implementação Singleton, com o objetivo de armazenar conjuntos de transações
 * que não sofrem mais alterações para o mesmo id cliente, ano e mês passados como parâmetro.
 * @author Cleison
 *
 */
public class CacheTransacaoCliente {
	private static CacheTransacaoCliente instance = null;
	private HashMap<Integer, HashMap<Integer, HashMap<Integer, List<Transacao>>>> cache; 
	
	private CacheTransacaoCliente() {
		this.cache = new HashMap<>();
	}
	
	public static CacheTransacaoCliente getInstance() {
		if (instance == null) {
			instance = new CacheTransacaoCliente();
		}
		
		return instance;
	}
	
	/**
	 * Guarda uma lista de transações para consultas futuras.
	 * @param idUsuario
	 * @param ano
	 * @param mes
	 * @param listaTransacoes
	 */
	public void salvar(Integer idUsuario, Integer ano, Integer mes, List<Transacao> listaTransacoes) {
		if(this.cache.get(idUsuario) == null) {
			HashMap<Integer, List<Transacao>> mesTransacoes = new HashMap<>();
			mesTransacoes.put(mes, listaTransacoes);
			
			HashMap<Integer, HashMap<Integer, List<Transacao>>> anoMes = new HashMap<>();
			anoMes.put(ano, mesTransacoes);
			
			this.cache.put(idUsuario, anoMes);
		} else if(this.cache.get(idUsuario).get(ano) == null) {
			HashMap<Integer, List<Transacao>> mesTransacoes = new HashMap<>();
			mesTransacoes.put(mes, listaTransacoes);
			
			this.cache.get(idUsuario).put(ano, mesTransacoes);			
		} else {
			this.cache.get(idUsuario).get(ano).put(mes, listaTransacoes);
		}
	}
	
	/**
	 * Retorna lista de transações ou null quando ainda  consta dados salvos para os parâmetros passados.
	 * @param idUsuario Integer
	 * @param ano Integer
	 * @param mes Integer
	 * @return List ou null quando não é encontrado dados. 
	 */
	public List<Transacao> obterListaTransacoes(Integer idUsuario, Integer ano, Integer mes) {
		List<Transacao> lista;
		try {
			lista = this.cache.get(idUsuario).get(ano).get(mes);
		} catch (Exception ex) {
			lista = null;
		}
		
		return lista;
	}

}
