package com.techclei.repositories;

import java.util.List;

import com.techclei.entities.Transacao;

public interface TransacaoRepository {
	
	public List<Transacao> obterListaTransacoes(Integer idUsuario, Integer ano, Integer mes);
	public void salvarListaTransacoes(Integer idUsuario, Integer ano, Integer mes, List<Transacao> listaTransacoes);
}
